<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="../css/style2.css">
  <link rel="stylesheetreset" href="../css/reset.css">
  <link rel="icon" href="favicon-16x16.png" type="image/gif" sizes="16x16">
  <title> Assignment 05 -- Balser, Justin</title>
</head>
<body style="background-image: url('tile.png')">

<div class="container">
<header>
  <h1><b>Three Column Layout</b></h1>
</header>

  <nav>
    <ul>
        <li><a href="#">One</a></li>
          <br>
        <li><a href="#">Two</a></li>
          <br>
        <li><a href="#">Three</a></li>
          <br>
        <li><a href="#">Four</a></li>
    </ul>
  </nav>

<main>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>
</main>

<aside>
<h2 style="text-align:center; color: darkorange">Four buttons</h2>

<figure class="container" style="justify-content:center">
  <img src="images/10.jpg" id="image">
    <br>
  <img src="images/20.jpg" id="image1">
    <br>
  <img src="images/30.jpg" id="image2">
    <br>
  <img src="images/40.jpg" id="image3">

<div>
<br>
<button onclick="shadowOn()">Shadow On</button>
<button onclick="shadowOff()">Shadow Off</button>
  <script type="text/javascript">
  function shadowOn() {
document.getElementById("image").style.boxShadow = "40px 37px 38px -20px rgba(0,0,0,0.69)";
document.getElementById("image1").style.boxShadow = "40px 37px 38px -20px rgba(0,0,0,0.69)";
document.getElementById("image2").style.boxShadow = "40px 37px 38px -20px rgba(0,0,0,0.69)";
document.getElementById("image3").style.boxShadow = "40px 37px 38px -20px rgba(0,0,0,0.69)";
}
</script>

<script type="text/javascript">
  function shadowOff() {
    document.getElementById("image").style.boxShadow = "none";
    document.getElementById("image1").style.boxShadow = "none";
    document.getElementById("image2").style.boxShadow = "none";
    document.getElementById("image3").style.boxShadow = "none";
}
</script>
</div>
</figure>
</aside>
</div>

<div id="footer">Copyright</div>
</body>
</html>
